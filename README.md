# Ocean Adventure Game

Ocean Adventure is an engaging underwater game where players dive into the ocean, manage oxygen levels, avoid obstacles, and participate in mini-games to collect power-ups and pearls. The game is developed using Godot and GDScript, offering a vibrant underwater environment and interactive gameplay.

## Table of Contents

- [Objective](#objective)
- [Game Overview](#game-overview)
- [Mini-Game: Splash 'n' Smack](#mini-game-splash-n-smack)
- [Gameplay Features](#gameplay-features)
- [Pearls as Collectibles](#pearls-as-collectibles)
- [Development Plan](#development-plan)
- [Progress and Challenges](#progress-and-challenges)
- [Contributing](#contributing)

## Objective

The main objective of Ocean Adventure is to navigate through an underwater world while maintaining oxygen levels by collecting oxygen cylinders, avoiding obstacles, and engaging in mini-games to earn power-ups and collectibles. Players aim to reach the end of the level with sufficient oxygen and power-ups to overcome challenges.

## Game Overview

In Ocean Adventure, players control a diver exploring the ocean depths. The game combines skillful navigation with strategic resource management, as players must collect oxygen cylinders to keep their oxygen levels up and engage in mini-games for power-ups. The game's vibrant graphics and immersive sound create a captivating experience.

## Mini-Game: Splash 'n' Smack

Splash 'n' Smack is a mini-game within Ocean Adventure, inspired by the classic whack-a-mole style. Players interact with sea creatures popping up on the screen to earn additional oxygen and power-ups. This mini-game adds an exciting layer of challenge and reward to the main gameplay.

## Gameplay Features

- **Player Selection Screen**: Choose your character before diving into the ocean.
- **Basic Player Movement**: Navigate underwater using intuitive keyboard controls.
- **Underwater Environment**: Explore a beautifully rendered ocean environment with dynamic backgrounds.
- **Oxygen Management System**: Collect oxygen cylinders to maintain levels and avoid running out.
- **Obstacles and Collisions**: Skillfully dodge obstacles to preserve oxygen and progress.
- **Puzzle Mechanism**: Participate in mini-games like Splash 'n' Smack for power-ups and rewards.
- **Power-Ups**: Obtain abilities to ease navigation and enhance protection.
- **Win/Lose Conditions**: Achieve the level goal before oxygen runs out to win.
- **User Interface (UI)**: Clear and informative displays of score, oxygen levels, and progress.
- **Sound and Music**: Engaging audio to enhance the underwater experience.

## Pearls as Collectibles

In addition to oxygen cylinders, players can collect pearls scattered throughout the game. Pearls offer bonus points and contribute to achieving certain win conditions. Pearls spawn randomly on the grid, and players must reach them before they disappear.

### Pearl Collection Implementation

- **Spawning Pearls**: Pearls appear at random intervals and locations.
- **Collecting Pearls**: Players collect pearls by colliding with them, earning points.
- **Win Condition**: Collect a set number of pearls to achieve specific game objectives.

## Development Plan

### Week 1

- **Day 1-2: Setup and Familiarization**
  - Install Unity and Visual Studio.
  - Explore Unity and C# through tutorials.

- **Day 3-4: Basic Game Structure**
  - Develop the ocean environment and player movement.
  - Implement oxygen cylinders and depletion mechanics.

- **Day 5-6: Obstacles and Collision Mechanics**
  - Design and implement obstacle mechanics and collision detection.
  - Set up oxygen thresholds and game-over logic.

- **Day 7: Mini-Game Design**
  - Design Splash 'n' Smack, focusing on sea creature interaction.

### Week 2

- **Day 8-9: Implementing Mini-Games**
  - Develop Splash 'n' Smack logic and integrate it into the game.

- **Day 10: Integrating Mini-Games**
  - Ensure seamless transition between main game and mini-game.

- **Day 11-12: Power-Ups and Game Flow**
  - Implement power-ups and integrate them into gameplay.

- **Day 13: Testing and Debugging**
  - Conduct playtesting, identify issues, and apply fixes.

- **Day 14: Final Polishing and Enhancements**
  - Enhance visuals, polish animations, and improve sound.

- **Additional 1-2 Days (Buffer)**
  - Allow time for final testing and debugging.

## Progress and Challenges

### Progress

- Parallax background setup complete.
- Start button linked to the play scene.
- Sprites split from sprite sheets.
- Species and obstacles scenes created with animations.
- Player movement and animations functional.
- Collision with oxygen bar implemented.
- Score, message, and timer labels displayed.
- Implement player bounce-back on screen edges.
- Ensure even obstacle spawning from both sides.
- Update font styles for UI elements.

### Challenges

- Oxygen reduction when hitting obstacles.
- Keeping the player within screen bounds.
- Increasing fish and obstacle variety while keeping them on-screen.
- Optional: Adding points collection for shells.

## Contributing

We welcome contributions to enhance Ocean Adventure! If you have suggestions or improvements, please fork the repository and submit a pull request.

