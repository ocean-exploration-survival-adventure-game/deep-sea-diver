extends CharacterBody2D

var speed: float = 200.0
var animation_playing: bool = false
var hurt_animation_playing: bool = false
var is_paused: bool = false  # To track game paused state

# Reference to the TextureProgressBar node
@onready var texture_progress_bar: TextureProgressBar = $CanvasLayer/Control/TextureProgressBar

# Reference to AnimatedSprite2D node
@onready var animated_sprite: AnimatedSprite2D = $AnimatedSprite2D

# Reference to Timer node
@onready var hurt_timer: Timer = $HurtTimer

# Reference to AlertPopup node
@onready var alert_popup: Popup = $AlertLayer/AlertPopup

# Reference to the oxygen bar collision node
@onready var oxygen_bar_collision: StaticBody2D = $CanvasLayer/Control/OxygenBarCollision

@onready var alert_sound: AudioStreamPlayer = $AlertSound
# Reference to AudioStreamPlayer node for collision sound
@onready var collision_sound: AudioStreamPlayer = $CollisionSound

# Set to keep track of collided obstacles
var collided_obstacles = {}

# Pearl collection variables
var pearls_collected = 0
var target_pearls = 20

func _ready():
	if texture_progress_bar:
		texture_progress_bar.value = 100
		update_progress_bar_color()
		print("Initial Color set to Green")
	else:
		print("TextureProgressBar node not found or not accessible.")

	if hurt_timer:
		hurt_timer.connect("timeout", _on_HurtTimer_timeout)
	else:
		print("HurtTimer node not found or not accessible.")

	if alert_popup:
		alert_popup.connect("popup_hide", _on_AlertPopup_hide)
	else:
		print("AlertPopup node not found or not accessible.")
	
	print("Viewport size:", get_viewport_rect().size)

	# Test audio playback in _ready()
	if collision_sound:
		print("Testing CollisionSound")
		collision_sound.play()
	else:
		print("CollisionSound node not found or not accessible.")
	connect("pearl_collected",_on_pearl_collected)

func _process(delta: float):
	if not is_paused:
		handle_movement()
		update_animation()

func handle_movement():
	velocity = Vector2.ZERO

	if texture_progress_bar.value > texture_progress_bar.max_value * 0.20:  # Check if oxygen is above 20%
		if Input.is_action_pressed("ui_right"):
			velocity.x += 1
		if Input.is_action_pressed("ui_left"):
			velocity.x -= 1
		if Input.is_action_pressed("ui_down"):
			velocity.y += 1
		if Input.is_action_pressed("ui_up"):
			velocity.y -= 1

		velocity = velocity.normalized() * speed

func _physics_process(delta: float):
	if not is_paused:
		move_and_slide()
		global_position = global_position.clamp(Vector2.ZERO, get_viewport_rect().size)

		for i in range(get_slide_collision_count()):
			var collision = get_slide_collision(i)
			if collision:
				var collider = collision.get_collider()
				if collider and collider != oxygen_bar_collision and collider.get_instance_id() not in collided_obstacles:
					print("Collision detected with ", collider.name)
					if collider.name == "Pearl":
						collect_pearl(collider)
					else:
						collided_obstacles[collider.get_instance_id()] = true
						reduce_progress_bar()
						play_collision_sound()
						play_hurt_animation()

func play_collision_sound():
	if collision_sound:
		print("Playing collision sound")
		collision_sound.play()

func reduce_progress_bar():
	if texture_progress_bar:
		texture_progress_bar.value -= texture_progress_bar.max_value * 0.20  # Reduce by 20%
		if texture_progress_bar.value < 0:
			texture_progress_bar.value = 0
		if texture_progress_bar.value != 0:
			print("TextureProgressBar value reduced to: ", texture_progress_bar.value)
			update_progress_bar_color()
		if texture_progress_bar.value <= texture_progress_bar.max_value * 0.20:
			show_alert_popup()

func update_progress_bar_color():
	if texture_progress_bar:
		var progress_ratio = texture_progress_bar.value / texture_progress_bar.max_value
		print("Progress Ratio:", progress_ratio)  # Debug print
		if progress_ratio <= 0.33:
			texture_progress_bar.add_theme_color_override("bar_color", Color(1, 0, 0))  # Red for 0-33%
			print("Color set to Red")  # Debug print
		elif progress_ratio <= 0.66:
			texture_progress_bar.add_theme_color_override("bar_color", Color(1, 1, 0))  # Yellow for 33-66%
			print("Color set to Yellow")  # Debug print
		else:
			texture_progress_bar.add_theme_color_override("bar_color", Color(0.4, 1.0, 0))  # Green RYB for 66-100%
			print("Color set to Green")  # Debug print
		print("Tint Color:", texture_progress_bar.get_theme_color("bar_color"))

func update_animation():
	if hurt_animation_playing:
		return

	if velocity.length() > 0:
		if not animation_playing:
			animated_sprite.play("swimming")
			animation_playing = true
	else:
		if animation_playing:
			animated_sprite.play("idle")
			animation_playing = false

func play_hurt_animation():
	if not hurt_animation_playing:
		animated_sprite.play("hurt")
		hurt_animation_playing = true
		if hurt_timer:
			var hurt_duration = animated_sprite.sprite_frames.get_frame_count("hurt") / animated_sprite.sprite_frames.get_animation_speed("hurt")
			hurt_timer.start(hurt_duration)
		else:
			print("HurtTimer node not found or not accessible.")

func _on_HurtTimer_timeout():
	hurt_animation_playing = false
	update_animation()

func show_alert_popup():
	if alert_popup:
		is_paused = true  # Pause the game
		alert_popup.popup_centered()
		play_alert_sound()  # Play the alert sound

func play_alert_sound():
	if alert_sound:
		alert_sound.play()

func _on_AlertPopup_hide():
	is_paused = false  # Resume the game
	texture_progress_bar.value = texture_progress_bar.max_value
	update_progress_bar_color()

func _on_button_pressed():
	get_tree().change_scene_to_file("res://Scenes/Mini Game/instructions_mini_game.tscn")

func _on_win_timer_timeout():
	texture_progress_bar.value = 100
	update_progress_bar_color()
	print("Oxygen level reset to 100%")
signal pearl_collected

func collect_pearl(pearl):
	pearls_collected += 1
	print("Pearls collected: ", pearls_collected)
	pearl.queue_free()
	# Uncomment the next line if you have a function to play a sound
	# play_collect_sound()
	if pearls_collected >= target_pearls:
		show_win_dialog()
	
	# Emit the pearl_collected signal
	emit_signal("pearl_collected")

func _on_pearl_collected():
	# Handle the pearl collected signal
	print("Pearl collected signal emitted.")


func show_win_dialog():
	is_paused = true
	print("Congratulations! You collected all the pearls!")
	# Implement logic to show a win dialog or transition to a win scene
