extends Node2D

const MAX_BIG_FISH = 15
const MAX_BIG_MINES = 15
const MAX_SMALL_MINES = 15
const MAX_PEARLS = 40

var enemy_types = [
	preload("res://Scenes/Obstacles/BigFish.tscn"),
	preload("res://Scenes/Obstacles/BigMine.tscn"),
	preload("res://Scenes/Obstacles/SmallMine.tscn"),
	preload("res://Scenes/Game/pearl.tscn")  # Add Pearl scene
]

var entity_counts = {
	"BigFish": 0,
	"BigMine": 0,
	"SmallMine": 0,
	"Pearl": 0  # Add entry for Pearl
}

func _ready():
	# Start spawning enemies and NPCs
	print("Ready: Starting spawn timer")
	spawn_timer()

func spawn_timer() -> void:
	while true:
		var spawn_delay = randf_range(0.05, 0.1)  # Adjusted delay for faster spawning
		#print("Spawning in", spawn_delay, "seconds")
		await(get_tree().create_timer(spawn_delay).timeout)
		spawn_random_entity()

func spawn_random_entity() -> void:
	var entity_scene = enemy_types[randi() % enemy_types.size()]
	var entity_type

	if entity_scene == preload("res://Scenes/Obstacles/BigFish.tscn"):
		entity_type = "BigFish"
		if entity_counts[entity_type] >= MAX_BIG_FISH:
			#print("Max BigFish reached. Current count:", entity_counts[entity_type])
			return
	elif entity_scene == preload("res://Scenes/Obstacles/BigMine.tscn"):
		entity_type = "BigMine"
		if entity_counts[entity_type] >= MAX_BIG_MINES:
			#print("Max BigMine reached. Current count:", entity_counts[entity_type])
			return
	elif entity_scene == preload("res://Scenes/Obstacles/SmallMine.tscn"):
		entity_type = "SmallMine"
		if entity_counts[entity_type] >= MAX_SMALL_MINES:
			#print("Max SmallMine reached. Current count:", entity_counts[entity_type])
			return
	elif entity_scene == preload("res://Scenes/Game/pearl.tscn"):
		entity_type = "Pearl"
		if entity_counts[entity_type] >= MAX_PEARLS:
			#print("Max Pearls reached. Current count:", entity_counts[entity_type])
			return

	var entity_instance = entity_scene.instantiate()
	add_child(entity_instance)

	entity_counts[entity_type] += 1

	# Print to debug
	#print("Spawning", entity_type)

	var screen_size = get_viewport_rect().size
	var spawn_position = Vector2(randf_range(0, screen_size.x), randf_range(0, screen_size.y))
	entity_instance.position = spawn_position

	#print("Spawned", entity_type, "at position:", spawn_position)

	if entity_instance.has_method("set_speed"):
		entity_instance.set_speed(randf_range(150, 300))
		print("Set speed for", entity_type)

	#if entity_instance.connect("entity_removed", Callable(self, "_on_entity_exit")) != OK:
	#	print("Failed to connect signal for", entity_type)

func _on_entity_exit(entity_type: String) -> void:
	print("Entity removed:", entity_type)
	entity_counts[entity_type] -= 1
	print("Updated counts after removal:", entity_counts)
