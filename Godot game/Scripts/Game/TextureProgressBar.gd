extends Control

var progress: float = 0.0
var speed: float = 10.0  # Speed at which the progress bar fills

@onready var texture_progress_bar: TextureProgressBar = $TextureProgressBar

func _ready():
	# Initialize the TextureProgressBar value to 0
	texture_progress_bar.value = progress
	print("TextureProgressBar initialized")

func _process(delta: float):
	# Increment progress based on the speed and delta time
	progress += speed * delta
	
	# Ensure the progress value stays within bounds
	if progress > 100:
		progress = 0  # Reset progress to 0 when it exceeds 100

	# Update the TextureProgressBar value
	texture_progress_bar.value = progress

	# Print progress to the console for debugging
	print("Progress: ", progress)

	# Change color based on progress value
	update_progress_color()

func update_progress_color():
	var progress_ratio = texture_progress_bar.value / 100.0

	# Debugging: Print the progress ratio
	print("Progress ratio: ", progress_ratio)

	# Example: Change color based on progress value
	if progress_ratio < 0.33:
		texture_progress_bar.modulate = Color(1.0, 0.0, 0.0)  # Red
	elif progress_ratio < 0.66:
		texture_progress_bar.modulate = Color(1.0, 1.0, 0.0)  # Yellow
	else:
		texture_progress_bar.modulate = Color(0.0, 1.0, 0.0)  # Green

	# Debugging: Print the current modulate color
	print("Current modulate color: ", texture_progress_bar.modulate)
