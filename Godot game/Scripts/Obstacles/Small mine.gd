extends CharacterBody2D

# Variables
var speed: float = 130.0
var change_direction_time: float = 1.0
var direction: Vector2 = Vector2.ZERO
var time_passed: float = 0.0

# Called when the node enters the scene tree for the first time
func _ready():
	randomize()
	change_direction()

# Called every frame. 'delta' is the elapsed time since the previous frame
func _process(delta: float):
	time_passed += delta
	if time_passed >= change_direction_time:
		change_direction()
		time_passed = 0.0
	
	velocity = direction * speed
	move_and_slide()

# Change to a new random direction
func change_direction():
	direction.x = randf() * 2 - 1 # Random value between -1 and 1
	direction.y = randf() * 2 - 1 # Random value between -1 and 1
	direction = direction.normalized()
