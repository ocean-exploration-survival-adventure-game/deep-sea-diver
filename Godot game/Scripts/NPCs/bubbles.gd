extends CharacterBody2D

# Variables
var speed: float = 50.0
var change_direction_time: float = 1.0
var direction: Vector2 = Vector2.ZERO
var time_passed: float = 0.0

# Screen boundaries
var screen_size: Vector2

# Called when the node enters the scene tree for the first time
func _ready():
	randomize()
	change_direction()
	screen_size = get_viewport_rect().size

# Called every frame. 'delta' is the elapsed time since the previous frame
func _process(delta: float):
	time_passed += delta
	if time_passed >= change_direction_time:
		change_direction()
		time_passed = 0.0
	
	# Set the velocity property
	velocity = direction * speed
	move_and_slide()
	update_animation()

# Change to a new random direction
func change_direction():
	direction.x = randf() * 2 - 1 # Random value between -1 and 1
	direction.y = randf() * 2 - 1 # Random value between -1 and 1
	direction = direction.normalized()

# Update the fish's animation based on movement
func update_animation():
	if velocity.length() > 0:
		$AnimatedSprite2D.play("bubbles")

