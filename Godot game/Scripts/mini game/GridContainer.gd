extends GridContainer

@onready var creature_scenes = [
	preload("res://Scenes/Mini Game/Sea Creatures/jelly_fish_lion.tscn"),
	preload("res://Scenes/Mini Game/Sea Creatures/jelly_fish-n.tscn"),
	preload("res://Scenes/Mini Game/Sea Creatures/clam.tscn"),
	preload("res://Scenes/Mini Game/Sea Creatures/octopus.tscn"),
	preload("res://Scenes/Mini Game/Sea Creatures/baiji.tscn")
]

@onready var creature_spawn_timer = $CreatureSpawnTimer
@onready var game_timer = $GameTimer
@onready var end_game_dialog = $EndGameDialog
@onready var dialog_label = $EndGameDialog/VBoxContainer/Label
@onready var continue_button = $EndGameDialog/VBoxContainer/HBoxContainer/ContinueButton
@onready var try_again_button = $EndGameDialog/VBoxContainer/HBoxContainer/TryAgainButton
@onready var quit_button = $EndGameDialog/VBoxContainer/HBoxContainer/QuitButton
@onready var hammer_hit_sound = $HammerHitSound
@onready var background_music = $BackgroundMusic  # Reference to the background music player

var score_label
var message_label
var timer_label

var score = 0
var target_score = 10
var game_duration = 30  # seconds
var time_left = game_duration

func _ready():
	# Initialize labels after scene tree is fully loaded
	var hud = get_parent().find_child("HUD")
	if hud:
		score_label = hud.find_child("ScoreLabel")
		message_label = hud.find_child("MessageLabel")
		timer_label = hud.find_child("TimerLabel")

	print("score_label path: HUD/ScoreLabel")
	print("message_label path: HUD/MessageLabel")
	print("timer_label path: HUD/TimerLabel")

	print("HUD children:", hud.get_children())
	randomize()  # Ensure the random number generator is seeded
	reset_game()
	creature_spawn_timer.connect("timeout", _on_creature_spawn_timer_timeout)
	game_timer.connect("timeout", _on_game_timer_timeout)

	print("Connecting continue_button signal")
	if continue_button:
		continue_button.connect("pressed", _on_continue_button_pressed)
	else:
		print("continue_button is null")

	print("Connecting try_again_button signal")
	if try_again_button:
		try_again_button.connect("pressed", _on_try_again_button_pressed)
	else:
		print("try_again_button is null")

	print("Connecting quit_button signal")
	if quit_button:
		quit_button.connect("pressed", _on_quit_button_pressed)
	else:
		print("quit_button is null")

	game_timer.wait_time = 0.8
	game_timer.start()
	print("Game started with a duration of", game_duration, "seconds.")

	# Hide default buttons of PopupDialog
	end_game_dialog.get_ok_button().hide()
	end_game_dialog.get_cancel_button().hide()

	print("ScoreLabel reference: ", score_label)
	print("MessageLabel reference: ", message_label)
	print("TimerLabel reference: ", timer_label)

	# Play background music
	if background_music:
		background_music.play()

func reset_game():
	score = 0
	time_left = game_duration
	update_score_label()
	update_timer_label()
	if message_label:
		message_label.text = ""
	creature_spawn_timer.start()
	print("Game reset. Score:", score, "Target Score:", target_score)

func _on_creature_spawn_timer_timeout():
	print("Spawn timer timeout")
	# Choose a random tile
	var random_index = randi() % get_child_count()
	var random_tile = get_child(random_index)
	print("Random tile chosen:", random_tile.name)

	# Choose a random creature scene
	var random_creature_scene = creature_scenes[randi() % creature_scenes.size()]
	var creature_instance = random_creature_scene.instantiate()

	# Ensure the instance is of type Area2D
	if creature_instance is Area2D:
		# Connect the input_event signal to detect clicks
		creature_instance.connect("input_event", _on_creature_input_event)

	# Add the creature to the random tile
	random_tile.add_child(creature_instance)
	print("Creature added to tile:", creature_instance.name)

	# Play the pop-up animation
	var anim_player = creature_instance.get_node("AnimationPlayer")
	if anim_player:
		anim_player.play("pop_up")
		print("Animation played for creature:", creature_instance.name)
	else:
		print("AnimationPlayer not found in creature instance:", creature_instance.name)

	# Create a timer to remove the creature after 1 second
	var removal_timer = Timer.new()
	removal_timer.wait_time = 1
	removal_timer.one_shot = true
	removal_timer.connect("timeout", Callable(self, "_on_removal_timer_timeout").bind(creature_instance))
	add_child(removal_timer)
	removal_timer.start()
	print("Removal timer started for creature:", creature_instance.name)

	# Restart the spawn timer
	creature_spawn_timer.start()

func _on_removal_timer_timeout(creature_instance):
	print("Removal timer timeout for creature:", creature_instance.name)
	# Remove the creature
	creature_instance.queue_free()

func _on_creature_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed:
		score += 1
		update_score_label()
		print("Creature clicked, score:", score)
		play_hammer_hit_sound()  # Play the hammer hit sound
		# Check for win condition
		if score >= target_score:
			game_timer.stop()
			creature_spawn_timer.stop()
			show_end_game_dialog(true)
			print("Player wins with score:", score)

func play_hammer_hit_sound():
	if hammer_hit_sound:
		hammer_hit_sound.play()

func update_score_label():
	print("Updating score label")
	if score_label:
		score_label.text = "Score: " + str(score)
		print("Score updated to:", score)
	else:
		print("score_label is null")

func update_timer_label():
	print("Updating timer label")
	if timer_label:
		timer_label.text = "Time: " + str(time_left)
		print("Timer updated to:", time_left)
	else:
		print("timer_label is null")

func _on_game_timer_timeout():
	time_left -= 1
	update_timer_label()
	if time_left <= 0:
		game_timer.stop()
		creature_spawn_timer.stop()
		if score >= target_score:
			show_end_game_dialog(true)
			print("Game over. Player wins with score:", score)
		else:
			show_end_game_dialog(false)
			print("Game over. Player loses with score:", score)
	else:
		game_timer.start(1)  # Restart the timer for another second

func show_end_game_dialog(is_winner):
	if is_winner:
		dialog_label.text = "You won! What would you like to do?"
		continue_button.show()
		try_again_button.hide()
	else:
		dialog_label.text = "You lost! What would you like to do?"
		continue_button.hide()
		try_again_button.show()
	end_game_dialog.show()

func _on_continue_button_pressed():
	change_scene_to_game()

func _on_try_again_button_pressed():
	reset_game()
	end_game_dialog.hide()

func _on_quit_button_pressed():
	get_tree().quit()

func change_scene_to_game():
	var scene_path = "res://Scenes/Game/game.tscn"
	if ResourceLoader.exists(scene_path):
		print("Changing scene to:", scene_path)
		get_tree().change_scene_to_file(scene_path)
	else:
		print("Scene path does not exist:", scene_path)
