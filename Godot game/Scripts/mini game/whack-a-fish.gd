extends Node2D

@onready var creature_scenes = [
	preload("res://jelly_fish_lion.tscn"),
	preload("res://jelly_fish-n.tscn"),
	preload("res://clam.tscn"),
	preload("res://octopus.tscn"),
	preload("res://baiji.tscn")
]

@onready var creature_spawn_timer = $CreatureSpawnTimer

func _ready():
	creature_spawn_timer.start()

func _on_creature_spawn_timer_timeout():
	# Choose a random tile
	var random_index = randi() % get_child_count()
	var random_tile = get_child(random_index)

	# Choose a random creature scene
	var random_creature_scene = creature_scenes[randi() % creature_scenes.size()]
	var creature_instance = random_creature_scene.instantiate()
	
	# Add the creature to the random tile
	random_tile.add_child(creature_instance)

	# Play the pop-up animation
	var anim_player = creature_instance.get_node("AnimationPlayer")
	anim_player.play("pop_up")

	# Create a timer to remove the creature after 1.5 seconds
	var removal_timer = Timer.new()
	removal_timer.wait_time = 1.5
	removal_timer.one_shot = true
	removal_timer.connect("timeout", Callable(self, "_on_removal_timer_timeout").bind(creature_instance))
	add_child(removal_timer)
	removal_timer.start()

	# Restart the spawn timer
	creature_spawn_timer.start()

func _on_removal_timer_timeout(creature_instance):
	# Remove the creature
	creature_instance.queue_free()
