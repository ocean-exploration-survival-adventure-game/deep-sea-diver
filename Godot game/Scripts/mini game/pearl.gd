extends Area2D

signal pearl_collected

func _ready():
	connect("body_entered", _on_body_entered)

func _on_body_entered(body):
	# When another body enters the area, free this Area2D node
	queue_free()
