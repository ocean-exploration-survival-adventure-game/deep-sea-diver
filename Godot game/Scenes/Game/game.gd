extends Node2D

var pearls_collected = 0
var target_pearls = 20

@onready var collect_sound: AudioStreamPlayer = $CollectSound
@onready var hud = $HUD

func _ready():
	pearls_collected = 0

func collect_pearl(pearl):
	pearls_collected += 1
	pearl.queue_free()
	play_collect_sound()
	hud.update_pearl_count(pearls_collected)
	if pearls_collected >= target_pearls:
		show_win_dialog()

func play_collect_sound():
	if collect_sound:
		collect_sound.play()

func show_win_dialog():
	get_tree().paused = true
	hud.show_win_message("Congratulations! You collected all the pearls!")
